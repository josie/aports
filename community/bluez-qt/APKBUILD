# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=bluez-qt
pkgver=5.108.0
pkgrel=2
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
pkgdesc="Qt wrapper for Bluez 5 DBus API"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/bluez-qt.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/bluez-qt-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Multiple tests either hang or fail completely

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/bluez-qt.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
715bd060728a95c9eb8cedd3bdb314b1c8195865fdaa8000ab5d44403ec5e3e635c877f5d5709a6407bfd62ee2e49df59db5e1c0bf19be774748d7b3bf0e93b0  bluez-qt-5.108.0.tar.xz
"
