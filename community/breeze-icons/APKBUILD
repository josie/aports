# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=breeze-icons
pkgver=5.108.0
pkgrel=1
pkgdesc="Breeze icon themes"
arch="noarch !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-3.0-or-later"
makedepends="
	extra-cmake-modules
	py3-lxml
	python3
	qt5-qtbase-dev
	samurai
	"
checkdepends="bash"
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/frameworks/breeze-icons.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/breeze-icons-$pkgver.tar.xz"

# Several KDE applications use icons not yet present in most themes
# We want to keep the possibility for users to not use the KDE provided
# breeze-icons theme however, as hopefully in the future this situation changes
# Thus let any theme that provides these icons provide "kde-icons" so the user
# retains their ability to choose their preferred theme
provides="kde-icons"
provider_priority=100

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/breeze-icons.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBINARY_ICONS_RESOURCE=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -E '(dupe|symlink)'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d540c938f4aae2d25074ce97ba9d3ec82514068192897bfa2c3387977778cac9e0753e182af6da4fefff5ff79bc68a71af6eaedbf37eba9c6392ccd0ba59612d  breeze-icons-5.108.0.tar.xz
"
