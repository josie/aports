# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=grantleetheme
pkgver=23.04.3
pkgrel=1
pkgdesc="KDE PIM mail related libraries"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by knewstuff
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	grantlee-dev
	ki18n-dev
	knewstuff-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/grantleetheme-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# grantleethemetest is broken
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
4402eaddb77e2167c12fc42f49820720ea0ce5b0438eb9b3f0513ea33bd9b575ee49c8f440a6f2d5a398f124e63eb2e2ae2a057ae81b56353c5bbbd5396f34d8  grantleetheme-23.04.3.tar.xz
"
