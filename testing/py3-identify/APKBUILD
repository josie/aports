# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-identify
_pyname=identify
pkgver=2.5.26
pkgrel=0
pkgdesc="File identification library for Python"
url="https://github.com/pre-commit/identify"
arch="noarch"
license="MIT"
depends="py3-ukkonen"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$_pyname-$pkgver.tar.gz::https://github.com/pre-commit/identify/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
4c17c7f40b14fd45649e0a6b6885e2d96a1ce3cf91fbc1092fdb9b92d7bf2ac35b0c04a24bb3e77f3188a3c8d00f57dbe19ca660e1e08b60cdb60385f340c4bd  identify-2.5.26.tar.gz
"
