# Contributor: Steven Guikal <void@fluix.one>
# Maintainer: Steven Guikal <void@fluix.one>
pkgname=py3-fastapi
pkgver=0.101.0
pkgrel=0
pkgdesc="Modern, high-performance, web framework for building APIs based on standard Python type hints"
url="https://github.com/tiangolo/fastapi"
arch="noarch !armhf !ppc64le" # limited by py3-starlette
license="MIT"
depends="
	py3-pydantic
	py3-starlette
	"
makedepends="
	py3-gpep517
	py3-hatchling
	py3-installer
	"
checkdepends="
	py3-dirty-equals
	py3-email-validator
	py3-flask
	py3-httpx
	py3-orjson
	py3-passlib
	py3-peewee
	py3-pytest
	py3-pytest-xdist
	py3-python-jose
	py3-python-multipart
	py3-sqlalchemy
	py3-trio
	py3-ujson
	py3-yaml
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/tiangolo/fastapi/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/fastapi-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# Depends on older SQLAlchemy and slated for removal:
	# https://github.com/tiangolo/fastapi/blob/dd4e78ca7b09abdf0d4646fe4697316c021a8b2e/requirements-tests.txt#L9
	.testenv/bin/python3 -m pytest -p no:warnings \
		--ignore-glob "tests/test_tutorial/*sql_databases" \
		--ignore tests/test_dependency_normal_exceptions.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
fe1c44bff5225f72912c5c86a8704287692964a5d11a9dbad42a40ee1af4197ac727017a3658176272b2ac82fc008b90ac3dea146ee5d193566ecab2fe7b67ce  py3-fastapi-0.101.0.tar.gz
"
