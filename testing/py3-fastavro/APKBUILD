# Maintainer: Gennady Feldman <gena01@gmail.com>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-fastavro
_pkgname=fastavro
pkgver=1.8.2
pkgrel=0
pkgdesc="Fast Avro for Python"
# Tests for optional zstd and snappy codecs require
# unpackaged python modules 'zstandard' and 'python-snappy'
options="!check"
url="https://github.com/fastavro/fastavro"
arch="all !x86" # _tz_ tests fail
license="MIT"
depends="python3"
makedepends="py3-setuptools python3-dev cython"
checkdepends="py3-pytest py3-numpy"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/fastavro/fastavro/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	FASTAVRO_USE_CYTHON=1 python3 setup.py build
}

check() {
	PYTHONPATH="$(echo $PWD/build/lib.*)" python3 -m pytest -v tests
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
e07d1912d594272a11bdc12d352555233ee33a6849512a7f90f1e0b72c64265da0201e715f33fc6bfd5dfd045a936fa52b5fbeef2b7cb5aff92a34cbd2205507  py3-fastavro-1.8.2.tar.gz
"
