# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=code-oss
pkgver=1.81.0
pkgrel=0
# get this from vscodium
_productjson=9a210dc96425be77fe85489347a76d975c87c211
pkgdesc="Visual Studio Code (OSS, with VSX)"
url="https://github.com/microsoft/vscode"
arch="aarch64 x86_64" # electron
license="MIT"
depends="electron ripgrep"
makedepends="
	imagemagick
	jq
	krb5-dev
	libsecret-dev
	libxkbfile-dev
	nodejs
	npm
	pngquant
	python3
	yarn
	"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	"
install="$pkgname.post-install"
source="$pkgname-$pkgver.tar.gz::https://github.com/microsoft/vscode/archive/refs/tags/$pkgver.tar.gz
	product-$_productjson.json::https://github.com/VSCodium/vscodium/raw/$_productjson/product.json
	launcher
	enable-extensions.patch
	no-git.patch
	no-res.patch.noauto
	electron21.patch
	webpack-hash.patch.noauto
	"
builddir="$srcdir/vscode-$pkgver"
options="!check net" # no tests (that make sense to run..)

export ELECTRON_SKIP_BINARY_DOWNLOAD=1
export PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD=1

prepare() {
	default_prepare
	# block husky
	git init .

	# electron 20+ workaround for bad gyp usage
	# XXX: LARGEFILE64 fixme
	export CFLAGS="$CFLAGS -D_LARGEFILE64_SOURCE"
	export CPPFLAGS="$CPPFLAGS -D_LARGEFILE64_SOURCE"
	export CXXFLAGS="$CXXFLAGS -std=c++17 -D_LARGEFILE64_SOURCE"

	yarn install --cache-folder "$srcdir" --frozen-lockfile

	patch -Np1 < "$srcdir"/webpack-hash.patch.noauto
	patch -Np1 < "$srcdir"/no-res.patch.noauto

	# patch out telemetry
	# backported from https://github.com/VSCodium/vscodium/blob/master/undo_telemetry.sh
	rg --no-ignore -l "\.data\.microsoft\.com" . \
		| grep -v "\.map\$" | xargs -t -n 1 -P ${JOBS:-2} sed -i -E "s|//[^/]+\.data\.microsoft\.com|//0\.0\.0\.0|g"

	# merge the product.json from the repo with one from vscodium repo
	# this fixes some extensions, including python
	cp product.json original_product.json
	jq -s '.[0] * .[1]' original_product.json "$srcdir"/product-$_productjson.json > product.json
}

build() {
	node --max_old_space_size=4096 ./node_modules/.bin/gulp vscode-linux-x64-min

	cd resources/linux

	mv code.png code-1024-x.png
	local size
	for size in 16 24 32 48 64 128 192 256 512; do
		convert code-1024-x.png -resize ${size}x$size code-$size-x.png
		< code-$size-x.png pngquant --speed 1 - > code-$size.png
	done
	< code-1024-x.png pngquant --speed 1 - > code-1024.png
}

package() {
	mkdir -p "$pkgdir"/usr/share/applications
	sed -e "s|@@NAME_LONG@@|Code - OSS|g" \
		-e "s|@@NAME_SHORT@@|Code - OSS|g" \
		-e "s|@@NAME@@|code-oss|g" \
		-e "s|@@EXEC@@|/usr/bin/code-oss|g" \
		-e "s|@@ICON@@|com.visualstudio.code.oss|g" \
		-e "s|@@URLPROTOCOL@@|code-oss|g" \
		resources/linux/code.desktop > "$pkgdir"/usr/share/applications/code-oss.desktop
	sed -e "s|@@NAME_LONG@@|Code - OSS|g" \
		-e "s|@@NAME_SHORT@@|Code - OSS|g" \
		-e "s|@@NAME@@|code-oss|g" \
		-e "s|@@EXEC@@|/usr/bin/code-oss|g" \
		-e "s|@@ICON@@|com.visualstudio.code.oss|g" \
		-e "s|@@URLPROTOCOL@@|code-oss|g" \
		resources/linux/code-url-handler.desktop > "$pkgdir"/usr/share/applications/code-oss-url-handler.desktop

	for size in 16 24 32 48 64 128 192 256 512 1024; do
		install -Dm644 resources/linux/code-$size.png \
			"$pkgdir"/usr/share/icons/hicolor/${size}x$size/apps/com.visualstudio.code.oss.png
	done

	mkdir -p "$pkgdir"/usr/share/metainfo
	sed -e "s|@@NAME_LONG@@|Code - OSS|g" \
		-e "s|@@NAME@@|code-oss|g" \
		-e "s|@@LICENSE@@|MIT|g" \
		resources/linux/code.appdata.xml > "$pkgdir"/usr/share/metainfo/code-oss.appdata.xml

	mkdir -p "$pkgdir"/usr/share/mime/packages
	sed -e "s|@@NAME_LONG@@|Code - OSS|g" \
		-e "s|@@NAME@@|code-oss|g" \
		resources/linux/code-workspace.xml > "$pkgdir"/usr/share/mime/packages/code-oss-workspace.xml

	mkdir -p "$pkgdir"/usr/share/bash-completion/completions
	sed -e "s|@@APPNAME@@|code-oss|g" \
		resources/completions/bash/code > "$pkgdir"/usr/share/bash-completion/completions/code-oss

	mkdir -p "$pkgdir"/usr/share/zsh/site-functions
	sed -e "s|@@APPNAME@@|code-oss|g" \
		resources/completions/zsh/_code > "$pkgdir"/usr/share/zsh/site-functions/_code-oss

	mkdir -p "$pkgdir"/usr/lib/code-oss/resources
	cp -a ../VSCode-linux-x64/resources/app "$pkgdir"/usr/lib/code-oss/resources/

	# disable update server
	sed -i "/updateUrl/d" "$pkgdir"/usr/lib/code-oss/resources/app/product.json

	# link to system rg
	ln -sfv /usr/bin/rg \
		"$pkgdir"/usr/lib/code-oss/resources/app/node_modules.asar.unpacked/@vscode/ripgrep/bin/rg

	install -Dm755 "$srcdir"/launcher "$pkgdir"/usr/bin/code-oss
}

sha512sums="
948997f6d11e66ac8e2de3b4ba36b359622818b09b5e3e3ceba27e40a356dbd66a180da934611bfe0a6e376bede4523f2f58e7abe76fc472da8d5ef99c4d6729  code-oss-1.81.0.tar.gz
1a5bd6f3d615f8a945fe94809b0caa951595c1a871a1fb59d8585d4d495a07c9a05762f515fcd4db3cde3167611e68ed8bc1e2f29f5e2ae803546616104e0890  product-9a210dc96425be77fe85489347a76d975c87c211.json
aa06b5721d3790f134f15ac19cd190ccc7ff9c2b6d873bd12e9c483ce697a4cc80ec0e8b0cfc458380b6c5ceb406989e4ff8c99a176638d0bdc32c823cd8c070  launcher
9f36c7fa6f0fd6a516f8e22c47f53013337985e59085bf1ea70165d42a513a92aa2a7a99ef0715e752c7190fd39ba703d405838e61bcfb60cabe47c421781eb4  enable-extensions.patch
f8d744ed29d4fa57bd00b916e689ea0f5ac5590910e369fa26c76bd2bc6ba2dd692c06f11342f8517667fb818caab7b4e349cd30c629be225bc9cea4e02ed3b9  no-git.patch
48e46bd2ec1490e456a9227c4d74eae5e5c6a7e48a98f8f463370bf60eb7fcb140337e3e02577dea1eabbc48174e2e3b31a4717a945d5dc21ccdbfd3a9807349  no-res.patch.noauto
ba323878ba188355b2e57b9f0828b05d6ae97a4562635f8e3477c9cb03754945bef7714a5d2eb3921e22331a170c8047a69db1fe8e4dda685f8adde4ed2c9efd  electron21.patch
0939ed0e39883b27ac13cdde2e1dbce506043997b6b52610d1f75560db58bac646d6f8fd6b909e8ad595aab6ed9f3777206e33d7fa5a58e0a7acc4c530b5d6b0  webpack-hash.patch.noauto
"
