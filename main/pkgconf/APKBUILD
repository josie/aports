# Maintainer: Ariadne Conill <ariadne@dereferenced.org>
pkgname=pkgconf
pkgver=2.0.0
pkgrel=0
pkgdesc="development framework configuration tools"
url="https://gitea.treehouse.systems/ariadne/pkgconf"
arch="all"
license="ISC"
replaces="pkgconfig"
provides="pkgconfig=1"
checkdepends="kyua atf"
subpackages="$pkgname-doc $pkgname-dev"
source="https://distfiles.ariadne.space/pkgconf/pkgconf-$pkgver.tar.xz
	"

# secfixes:
#   1.9.4-r0:
#     - CVE-2023-24056

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-pkg-config-dir=/usr/local/lib/pkgconfig:/usr/local/share/pkgconfig:/usr/lib/pkgconfig:/usr/share/pkgconfig
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	ln -s pkgconf "$pkgdir"/usr/bin/pkg-config
}

dev() {
	default_dev

	# Move pkg-config back to main package (default_dev implicitly moves
	# files /usr/bin/*-config to -dev).
	mv "$subpkgdir"/usr/bin/pkg-config "$pkgdir"/usr/bin/

	mkdir -p "$pkgdir"/usr/share/aclocal/
	mv "$subpkgdir"/usr/share/aclocal/pkg.m4 "$pkgdir"/usr/share/aclocal/
}

sha512sums="
6f40201a1e5d400358bb5218647d3160d42881c4a7d1bc3c8a75b306e47dc2ed1204268b2bdac0c14068e66cd3defe211570263274de7fedd0206b7dd343613d  pkgconf-2.0.0.tar.xz
"
